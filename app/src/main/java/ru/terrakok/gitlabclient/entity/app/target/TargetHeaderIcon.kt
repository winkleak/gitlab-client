package ru.terrakok.gitlabclient.entity.app.target

/**
 * Created by Eugene Shapovalov (@CraggyHaggy) on 19.11.17.
 */
enum class TargetHeaderIcon {
    CREATED,
    JOINED,
    COMMENTED,
    MERGED,
    CLOSED,
    DESTROYED,
    EXPIRED,
    LEFT,
    REOPENED,
    PUSHED,
    UPDATED,
    NONE
}